# Cassiopée

Un dispositif connecté, pour poétiser les données environnementales de notre quotidien.

Projet issu du MétroMix 2019 du LabFab de Rennes Métropole, voir la documentation sur [Wiki-Rennes](http://www.wiki-rennes.fr/M%C3%A9troMix_2019/Cassiop%C3%A9e)


## Principe technique
Un serveur Node-RED collecte régulièrement des données via des API web publiques, puis en fait une synthèse, transmise à une sculpture connectée, qui s'anime et se colore selon les données reçues.

Quatre points de données sont obtenus : passage de vélos place de Bretagne, trafic rocade de Rennes, mesure NOX d'AirBreizh, mesure PM2.5 d'un capteur citoyen rennais.

La transmission vers la sculpture est prévue en MQTT, vers un composant connecté de type esp8266.

## Usage
Pour utiliser le code de ce repo, il suffit d'importer le fichier dans votre IDE web de Node-RED. L'ensemble des noeuds sera ainsi affiché, et disponible modification.

L'interface "web UI" est utilisée pour voir le résultat de la moulinette directement sur le web, même sans sculpture à disposition. Un bouton de "pilotage/simulation" est également implémenté.

## Licence
Le code du projet est diffusé sous licence CC-BY-SA.
