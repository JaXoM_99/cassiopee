#ifndef __RGBSTRIP_H
#define __RGBSTRIP_H

#include <Adafruit_NeoPixel.h>

int rgbstrip_getnbled ();
int rgbstrip_getnbstrip ();
void rgbstrip_setallstripcolor (int ipx, int R,int G, int B);
void rgbstrip_setallstripcolor (int ipx, uint32_t color);
void rgbstrip_allstripshow ();
void rgbstrip_allstripbegin ();

Adafruit_NeoPixel* rgbstrip_getstrip (int n);



#endif
