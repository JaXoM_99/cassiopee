#include <ESP8266WiFi.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic


#include <Wire.h>
//#include <SPI.h>



#include <WiFiUdp.h>
#include <PubSubClient.h>

#include <Servo.h>
#include <Adafruit_NeoPixel.h>

#include "rgbstrip.h"

#define DEBUG_LOG 1

#define LED_BUILTIN 2
#define servo1Pin D3 // Define the NodeMCU pin to attach the Servo



const char* ssid = "cassiopee";
const char* password = "cassiopee";
const char* mqtt_server = "155.133.131.224";
const char* mqtt_username = "sysop";
const char* mqtt_password = "cassiopee";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[128];
char client_id[64];
int value = 0;
char topic[64];
int led_state = 1;
int change = 0;

WiFiUDP wifiUdp;

Servo servo1;        // Create the Servo and name it "servo1"


void(* resetFunc) (void) = 0;//declare reset function at address 0


void logprint (const char* p)
{
  #ifdef DEBUG_LOG
  if (Serial)
    Serial.print (p);
  #endif

}
void logprint (int p)
{
  #ifdef DEBUG_LOG
  if (Serial)
    Serial.print (p);
  #endif
}
void logprint (float p)
{
  #ifdef DEBUG_LOG
  if (Serial)
    Serial.print (p);
  #endif  
}
void logprintln (const char* p)
{
  #ifdef DEBUG_LOG
  if (Serial)
    Serial.println (p);
  #endif
}

void setup_wifi() {

  // We start by connecting to a WiFi network

  logprintln("");
  logprint("Connecting to ");
  logprintln(ssid);
  

  //WiFi.begin(ssid, password);
  WiFiManager wifiManager;
  //wifiManager.resetSettings(); //debug
  
  int request = 0;

  //while (WiFi.status() != WL_CONNECTED) {
  while (!wifiManager.autoConnect(ssid, password)) {

    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    delay(125);
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
    delay(125);
   
    logprint(".");
    

    request++;

    if (request > 4 * 60)
    {
      for (int i = 0; i < rgbstrip_getnbled(); i++)
        rgbstrip_setallstripcolor (i, 255, 0, 0);
      rgbstrip_allstripshow ();  
      delay (250);
      ESP.reset ();
    }
    else
    {
      for (int i = 0; i < rgbstrip_getnbled(); i++)
        rgbstrip_setallstripcolor (i, 0, 0, 0);
      rgbstrip_setallstripcolor (request % rgbstrip_getnbled(), 0,0,64);

      rgbstrip_allstripshow ();  
    }
      
  }
    // initialize topic root
  snprintf (topic, sizeof(topic), "/dev/%s", WiFi.macAddress().c_str());

  rgbstrip_setallstripcolor(0, 0, 255, 0);
  rgbstrip_setallstripcolor(4, 0, 0, 0);

  rgbstrip_allstripshow ();

  
  logprintln("");
  logprintln("WiFi connected");
  logprintln("IP address: ");
  logprintln(WiFi.localIP().toString().c_str());
}



void callback(char* topic, byte* payload, unsigned int length) {
  
  logprint("Message arrived [");
  logprint(topic);
  logprint("] ");
  payload[length] = '\0';
  if (strstr (topic, "mouv") != NULL)
  {
    //logprintln ((int) length);
    logprintln ((char*) payload);
    int p = atoi ((char*)payload);
    if (p > 100)
      p = 100;
    p= (p * 255) / 100;
    servo1.write(p);
    delay (250);
  }
  else if (strstr(topic, "coul") != NULL)
  {
    int val =atoi((char*)payload);

    if (val > 100)
      val = 100;

    int red = 64;
    int blue = 0;
    int green = val * 255 / 100;
    
    if (val < 20)
      red = 255;
    for (int i = 0; i < rgbstrip_getnbled(); i++)
        rgbstrip_setallstripcolor (i, red, green, blue);
        
    rgbstrip_allstripshow ();
  }
  else if (strstr(topic, "RVB") != NULL) 
  {
    int r,v,b;
    sscanf ((char*) payload, "%d %d %d", &r,&v,&b);
    for (int i = 0; i < rgbstrip_getnbled(); i++)
      rgbstrip_setallstripcolor (i, r, v, b);
    rgbstrip_allstripshow ();
  }     
 
  logprint ((char*) payload);
  logprintln("|");
 
  change++;

}

void reconnect() {
  // Loop until we're reconnected
  char willtopic[64];
  
  while (!client.connected()) {
    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    
    logprint("Attempting MQTT connection...");
    
    // Attempt to connect
    client.setServer(mqtt_server, 1883);
    
    client.setCallback(callback);

    snprintf (willtopic, sizeof(willtopic), "%s/status", topic);
    
    if (client.connect(client_id, mqtt_username, mqtt_password, willtopic, 2, true, "DISCONNECTED"))
    {
      logprintln("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("/Metromix/mouv");
      client.subscribe("/Metromix/coul");
      client.subscribe("/Metromix/RVB");
      digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
      client.publish(willtopic, "CONNECTED", true);
      logprintln("CONNECTED to mqtt");

    } else {
      logprint("failed, rc=");
      logprint(client.state());
      logprintln(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
      delay(5000);
    }
  }
  digitalWrite(LED_BUILTIN, led_state == 1 ? LOW :  HIGH);   // Turn the LED acordingly
}


void setup() {
  #ifdef DEBUG_LOG
  Serial.begin(9600);
  Serial.println ("start.........");
  #endif

  

  // initialize servo
  servo1.attach(servo1Pin);

  // initialize led strips
  rgbstrip_allstripbegin ();

  rgbstrip_setallstripcolor (4, 255, 0, 0);
  rgbstrip_allstripshow ();

  delay(250);

  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  digitalWrite (LED_BUILTIN, HIGH);
  delay (50);
  digitalWrite (LED_BUILTIN, LOW);
  delay (1000);
  
  setup_wifi();
  

  digitalWrite(LED_BUILTIN, led_state == 1 ? LOW :  HIGH);   // Turn the LED acordingly
  for (int i = 0; i < rgbstrip_getnbled(); i++)
        rgbstrip_setallstripcolor (i, 0, 64, 0);
  rgbstrip_allstripshow ();  
  delay (500);
  for (int i = 0; i < rgbstrip_getnbled(); i++)
        rgbstrip_setallstripcolor (i, 0, 0, 0);
    rgbstrip_setallstripcolor (3, 0,0,64);
    rgbstrip_setallstripcolor (4, 0,64,0);

  rgbstrip_allstripshow ();  
}
  
void loop() {
   
  if (!client.connected()) 
  {
       reconnect();
  }
  client.loop();
  
  digitalWrite(LED_BUILTIN, led_state == 1 ? LOW :  HIGH);   // Turn the LED acordingly

  delay (100);

  long now = millis();
    
  //logprintln ("-------------------------------------------");


  lastMsg = now;
    
}
