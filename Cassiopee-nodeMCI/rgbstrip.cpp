//#include <arduino.h>


#include "rgbstrip.h"

#define NB_LED 21

Adafruit_NeoPixel stripa[] ={ 
  Adafruit_NeoPixel(NB_LED, D5, NEO_GRB + NEO_KHZ800 ),
  Adafruit_NeoPixel(NB_LED, D6, NEO_GRB + NEO_KHZ800),
  Adafruit_NeoPixel(NB_LED, D7, NEO_GRB + NEO_KHZ800),
   Adafruit_NeoPixel(NB_LED, D8, NEO_GRB + NEO_KHZ800)
};

#define N_STRIP  ((int) ( sizeof(stripa)/ sizeof(stripa[0]) ))


int rgbstrip_getnbled ()
{
    return (NB_LED);
}

int rgbstrip_getnbstrip ()
{
    return (N_STRIP);
}

Adafruit_NeoPixel* rgbstrip_getstrip (int n)
{
    if (n >= N_STRIP)
        return (NULL);

    return (&stripa[n]);
}

void rgbstrip_allstripbegin ()
{
    for (int s =0; s < N_STRIP; s++)
        stripa[s].begin ();
}
void rgbstrip_allstripshow ()
{
  for (int s = 0; s < N_STRIP; s++)
    stripa[s].show ();
}

void rgbstrip_setallstripcolor (int ipx, int R,int G, int B)
{
  for (int s = 0; s < N_STRIP; s++)
  {
      stripa[s].setPixelColor (ipx, stripa[s].Color (R, G, B));
  }
}

void rgbstrip_setallstripcolor (int ipx, uint32_t color)
{
  for (int s = 0; s < N_STRIP; s++)
  {
      stripa[s].setPixelColor (ipx, color);
  }
}